/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_H
#define C_INCLUDE_DRAWING_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_canvas.h
 *
 * @brief Declares the functions related to the canvas in the drawing module.
 *
 * File to include: native_drawing/drawing_canvas.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the constraint types of the source rectangle.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_SrcRectConstraint {
    /** The source rectangle must be completely contained in the image. */
    STRICT_SRC_RECT_CONSTRAINT,
    /** The source rectangle can be partly outside the image. */
    FAST_SRC_RECT_CONSTRAINT,
} OH_Drawing_SrcRectConstraint;

/**
 * @brief Creates an <b>OH_Drawing_Canvas</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Canvas</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Canvas* OH_Drawing_CanvasCreate(void);

/**
 * @brief Destroys an <b>OH_Drawing_Canvas</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDestroy(OH_Drawing_Canvas*);

/**
 * @brief Binds a bitmap to a canvas so that the content drawn on the canvas is output to the bitmap.
 * (This process is called CPU rendering.)
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasBind(OH_Drawing_Canvas*, OH_Drawing_Bitmap*);

/**
 * @brief Attaches a pen to a canvas so that the canvas can use the style and color of the pen to outline a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachPen(OH_Drawing_Canvas*, const OH_Drawing_Pen*);

/**
 * @brief Detaches the pen from a canvas so that the canvas can no longer use the style and color of the pen
 * to outline a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachPen(OH_Drawing_Canvas*);

/**
 * @brief Attaches a brush to a canvas so that the canvas can use the style and color of the brush to fill in a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Brush Pointer to an <b>OH_Drawing_Brush</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachBrush(OH_Drawing_Canvas*, const OH_Drawing_Brush*);

/**
 * @brief Detaches the brush from a canvas so that the canvas can no longer use the style and color of the brush
 * to fill in a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachBrush(OH_Drawing_Canvas*);

/**
 * @brief Saves the current canvas status (canvas matrix) to the top of the stack.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasSave(OH_Drawing_Canvas*);

/**
 * @brief Saves the matrix and cropping region, and allocates a bitmap for subsequent drawing. If you call
 * {@link OH_Drawing_CanvasRestore}, the changes made to the matrix and clipping region are discarded,
 * and the bitmap is drawn.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @param OH_Drawing_Brush Pointer to an {@link OH_Drawing_Brush} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSaveLayer(OH_Drawing_Canvas*, const OH_Drawing_Rect*, const OH_Drawing_Brush*);

/**
 * @brief Restores the canvas status (canvas matrix) saved on the top of the stack.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasRestore(OH_Drawing_Canvas*);

/**
 * @brief Obtains the number of canvas statuses (canvas matrices) saved in the stack.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @return Returns a 32-bit value that describes the number of canvas statuses (canvas matrices).
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_CanvasGetSaveCount(OH_Drawing_Canvas*);

/**
 * @brief Restores to a given number of canvas statuses (canvas matrices).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param saveCount Number of canvas statuses (canvas matrices).
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRestoreToCount(OH_Drawing_Canvas*, uint32_t saveCount);

/**
 * @brief Draws a line segment.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param x1 X coordinate of the start point of the line segment.
 * @param y1 Y coordinate of the start point of the line segment.
 * @param x2 X coordinate of the end point of the line segment.
 * @param y2 Y coordinate of the end point of the line segment.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawLine(OH_Drawing_Canvas*, float x1, float y1, float x2, float y2);

/**
 * @brief Draws a path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPath(OH_Drawing_Canvas*, const OH_Drawing_Path*);

/**
 * @brief Draws a portion of a pixel map onto a specified area of the canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_PixelMap Pointer to an {@link OH_Drawing_PixelMap} object.
 * @param src Pointer to a rectangle on the pixel map. This parameter can be left empty.
 * @param dst Pointer to a rectangle on the canvas.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPixelMapRect(OH_Drawing_Canvas*, OH_Drawing_PixelMap*, const OH_Drawing_Rect* src,
    const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*);

/**
 * @brief Draws a background filled with a brush.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Brush Pointer to an <b>OH_Drawing_Brush</b> object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBackground(OH_Drawing_Canvas*, const OH_Drawing_Brush*);

/**
 * @brief Draws a region.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Region Pointer to an <b>OH_Drawing_Region</b> object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRegion(OH_Drawing_Canvas*, const OH_Drawing_Region*);

/**
 * @brief Enumerates the modes of drawing multiple points.
 * The modes include discrete points, line segments, and open polygons.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PointMode {
    /**
     * Draws each point separately.
     */
    POINT_MODE_POINTS,
    /**
     * Draws every two points as a line segment.
     */
    POINT_MODE_LINES,
     /**
     * Draws an array of points as an open polygon.
     */
    POINT_MODE_POLYGON,
} OH_Drawing_PointMode;

/**
 * @brief Draws multiple points. You can draw a single point, a line segment, or an open polygon.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param mode Mode of drawing multiple points.
 * For details about the available options, see {@link OH_Drawing_PointMode}.
 * @param count Number of vertices, that is, the number of vertices in the vertex array.
 * @param OH_Drawing_Point2D Pointer to an array holding the vertices.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPoints(OH_Drawing_Canvas*, OH_Drawing_PointMode mode,
    uint32_t count, const OH_Drawing_Point2D*);

/**
 * @brief Draws a bitmap. A bitmap, also referred to as a dot matrix image, a pixel map image, or a grid image,
 * includes single points called pixels (image elements).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @param left X coordinate of the upper left corner of the bitmap.
 * @param top Y coordinate of the upper left corner of the bitmap.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBitmap(OH_Drawing_Canvas*, const OH_Drawing_Bitmap*, float left, float top);

/**
 * @brief Draws a portion of a bitmap onto a specified area of the canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Bitmap Pointer to an {@link OH_Drawing_Bitmap} object.
 * @param src Pointer to a rectangle on the bitmap. This parameter can be left empty.
 * @param dst Pointer to a rectangle on the canvas.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBitmapRect(OH_Drawing_Canvas*, const OH_Drawing_Bitmap*, const OH_Drawing_Rect* src,
    const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*);

/**
 * @brief Sets the matrix status for a canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object,
 * which is obtained by calling {@link OH_Drawing_MatrixCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSetMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief Resets the matrix of a canvas to an identity matrix.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasResetMatrix(OH_Drawing_Canvas*);

/**
 * @brief Draws a portion of an image onto a specified area of the canvas.
 * The area selected by the source rectangle is scaled and translated to the destination rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @param src Pointer to a source rectangle, which is an {@link OH_Drawing_Rect} object.
 * @param dst Pointer to a destination rectangle, which is an {@link OH_Drawing_Rect} object.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * @param OH_Drawing_SrcRectConstraint Constraint type.
 * For details about the available options, see {@link OH_Drawing_SrcRectConstraint}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawImageRectWithSrc(OH_Drawing_Canvas*, const OH_Drawing_Image*,
    const OH_Drawing_Rect* src, const OH_Drawing_Rect* dst, const OH_Drawing_SamplingOptions*,
    OH_Drawing_SrcRectConstraint);

/**
 * @brief Draws an image onto a specified area of the canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawImageRect(OH_Drawing_Canvas*, OH_Drawing_Image*,
    OH_Drawing_Rect* dst, OH_Drawing_SamplingOptions*);

/**
 * @brief Enumerates the modes of interpreting the geometry of a given vertex.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_VertexMode {
    /**
     * Draws a triangle list. Specifically, a list of isolated triangles are drawn using every three vertices.
     * If the number of vertices is not a multiple of 3, the extra vertices will be ignored.
     */
    VERTEX_MODE_TRIANGLES,
    /**
     * Draws a triangle strip. Specifically, the first triangle is drawn between the first 3 vertices,
     * and all subsequent triangles use the previous 2 vertices plus the next additional vertex.
     */
    VERTEX_MODE_TRIANGLESSTRIP,
    /**
     * Draws a triangle fan. A triangle fan is similar to a triangle strip, except that all the triangles share
     * one vertex (the first vertex).
     */
    VERTEX_MODE_TRIANGLEFAN,
} OH_Drawing_VertexMode;

/**
 * @brief Draws a triangular grid described by a vertex array.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param vertexMmode Vertex drawing mode. For details about the available options, see {@link OH_Drawing_VertexMode}.
 * @param vertexCount Number of vertices in the vertex array.
 * @param positions Pointer to the position array.
 * @param texs Pointer to the texture coordinate array.
 * @param colors Pointer to the color array.
 * @param indexCount Number of indices.
 * @param indices Pointer to the index array.
 * @param mode Blend mode. For details about the available options, see {@link OH_Drawing_BlendMode}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawVertices(OH_Drawing_Canvas*, OH_Drawing_VertexMode vertexMmode,
    int32_t vertexCount, const OH_Drawing_Point2D* positions, const OH_Drawing_Point2D* texs,
    const uint32_t* colors, int32_t indexCount, const uint16_t* indices, OH_Drawing_BlendMode mode);

/**
 * @brief Copies pixel data from a canvas to a specified address. This function cannot be used for recorded canvases.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Image_Info Pointer to an {@link OH_Drawing_Image_Info} object.
 * @param dstPixels Pointer to the start address for storing the pixel data.
 * @param dstRowBytes Size of pixels per row.
 * @param srcX X-axis offset of the pixels on the canvas, in px.
 * @param srcY Y-axis offset of the pixels on the canvas, in px.
 * @return Returns <b>true</b> if the pixel data is copied to the start address of the storage;
 * returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_CanvasReadPixels(OH_Drawing_Canvas*, OH_Drawing_Image_Info*,
    void* dstPixels, uint32_t dstRowBytes, int32_t srcX, int32_t srcY);

/**
 * @brief Copies pixel data from a canvas to an image. This function cannot be used for recorded canvases.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Bitmap Pointer to an {@link OH_Drawing_Bitmap} object.
 * @param srcX X-axis offset of the pixels on the canvas, in px.
 * @param srcY Y-axis offset of the pixels on the canvas, in px.
 * @return Returns <b>true</b> if the pixel data is copied to the image; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_CanvasReadPixelsToBitmap(OH_Drawing_Canvas*, OH_Drawing_Bitmap*, int32_t srcX, int32_t srcY);

/**
 * @brief Draws a rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief Draws a circle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Point Pointer to an <b>OH_Drawing_Point</b> object, which indicates the center of the circle.
 * @param radius Radius of the circle.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawCircle(OH_Drawing_Canvas*, const OH_Drawing_Point*, float radius);

/**
 * @brief Draws an oval.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawOval(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief Draws an arc.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param startAngle Start angle of the arc.
 * @param sweepAngle Sweep angle of the arc.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawArc(OH_Drawing_Canvas*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief Draws a rounded rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRoundRect(OH_Drawing_Canvas*, const OH_Drawing_RoundRect*);

/**
 * @brief Draws a text blob.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_TextBlob Pointer to an <b>OH_Drawing_TextBlob</b> object.
 * @param x X coordinate of the lower left corner of the text blob.
 * @param y Y coordinate of the lower left corner of the text blob.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawTextBlob(OH_Drawing_Canvas*, const OH_Drawing_TextBlob*, float x, float y);

/**
 * @brief Enumerates the canvas clipping modes.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_CanvasClipOp {
    /**
     * Clips a specified area. That is, the difference set is obtained.
     */
    DIFFERENCE,
    /**
     * Retains a specified area. That is, the intersection is obtained.
     */
    INTERSECT,
} OH_Drawing_CanvasClipOp;

/**
 * @brief Clips a rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to enable anti-aliasing. The value <b>true</b> means to enable anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Clips a rounded rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to perform anti-aliasing. The value <b>true</b> means to perform anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasClipRoundRect(OH_Drawing_Canvas*, const OH_Drawing_RoundRect*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Clips a path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to enable anti-aliasing. The value <b>true</b> means to enable anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipPath(OH_Drawing_Canvas*, const OH_Drawing_Path*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Rotates a canvas by a given angle. A positive number indicates a clockwise rotation,
 * and a negative number indicates a counterclockwise rotation.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param degrees Angle to rotate.
 * @param px X coordinate of the rotation point.
 * @param py Y coordinate of the rotation point.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRotate(OH_Drawing_Canvas*, float degrees, float px, float py);

/**
 * @brief Translates a canvas by a given distance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param dx Distance to translate on the X axis.
 * @param dy Distance to translate on the Y axis.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasTranslate(OH_Drawing_Canvas*, float dx, float dy);

/**
 * @brief Scales a canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param sx Scale factor on the X axis.
 * @param sy Scale factor on the Y axis.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasScale(OH_Drawing_Canvas*, float sx, float sy);

/**
 * @brief Skews the canvas in both the horizontal and vertical directions.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param sx Amount of tilt on the X axis. A positive value means that the canvas tilts to the lower right,
 * and a negative value means that the canvas tilts to the lower left.
 * @param sy Amount of tilt on the Y axis. A positive value means that the canvas tilts to the upper right,
 * and a negative value means that the canvas tilts to the upper left.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasSkew(OH_Drawing_Canvas*, float sx, float sy);

/**
 * @brief Clears a canvas by using a given color.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param color Color, which is a 32-bit (ARGB) variable.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasClear(OH_Drawing_Canvas*, uint32_t color);

/**
 * @brief Obtains the canvas width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @return Returns the canvas width, in px.
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_CanvasGetWidth(OH_Drawing_Canvas*);

/**
 * @brief Obtains the canvas width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @return Returns the canvas height, in px.
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_CanvasGetHeight(OH_Drawing_Canvas*);

/**
 * @brief Obtains the bounds of the cropping region of a canvas. This function cannot be used for recorded canvases.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object,
 * which is obtained by calling {@link OH_Drawing_RectCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasGetLocalClipBounds(OH_Drawing_Canvas*, OH_Drawing_Rect*);

/**
 * @brief Obtains the 3x3 matrix of a canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object,
 * which is obtained by calling {@link OH_Drawing_MatrixCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasGetTotalMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief Preconcats the existing matrix with the passed-in matrix.
 * The drawing operation triggered before this function is called is not affected.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>{@link OH_Drawing_Canvas}</b> object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasConcatMatrix(OH_Drawing_Canvas*, OH_Drawing_Matrix*);

/**
 * @brief Enumerates the canvas shadow flags.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_CanvasShadowFlags {
    /**
     * There is no shadow flag.
     */
    SHADOW_FLAGS_NONE,
    /**
     * The occluding object is transparent.
     */
    SHADOW_FLAGS_TRANSPARENT_OCCLUDER,
    /**
     * No analysis on the shadows is required.
     */
    SHADOW_FLAGS_GEOMETRIC_ONLY,
    /**
     * All the preceding shadow flags are used.
     */
    SHADOW_FLAGS_ALL,
} OH_Drawing_CanvasShadowFlags;

/**
 * @brief Draws an offset spot shadow and uses a given path to outline the ambient shadow.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an {@link OH_Drawing_Canvas} object.
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object, which is used to generate the shadow.
 * @param planeParams Value of the function that returns the Z-axis of the occluding object from the canvas based on
 * the x-axis and y-axis.
 * @param devLightPos Position of the light relative to the canvas.
 * @param lightRadius Radius of the light.
 * @param ambientColor Color of the ambient shadow.
 * @param spotColor Color of the spot shadow.
 * @param flag Shadow flag. For details about the available options, see {@link OH_Drawing_CanvasShadowFlags}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_CanvasDrawShadow(OH_Drawing_Canvas*, OH_Drawing_Path*, OH_Drawing_Point3D planeParams,
    OH_Drawing_Point3D devLightPos, float lightRadius, uint32_t ambientColor, uint32_t spotColor,
    OH_Drawing_CanvasShadowFlags flag);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
