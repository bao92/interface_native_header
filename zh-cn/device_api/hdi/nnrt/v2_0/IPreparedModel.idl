/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup NNRt
 * @{
 *
 * @brief NNRt（Neural Network Runtime, 神经网络运行时）是面向AI领域的跨芯片推理计算运行时，作为中间桥梁连通上层AI推理框架和底层加速芯片，实现AI模型的跨芯片推理计算。提供统一AI芯片驱动接口，实现AI芯片驱动接入OpenHarmony。
 *
 * @since 3.2
 * @version 2.0
 */

/**
 * @file IPreparedModel.idl
 *
 * @brief 该文件定义了AI模型推理、获取模型输入tensor维度范围、导出编译后模型等接口。
 *
 * 模块包路径：ohos.hdi.nnrt.v2_0
 *
 * 引用：ohos.hdi.nnrt.v2_0.NnrtTypes
 *
 * @since 3.2
 * @version 2.0
 */

package ohos.hdi.nnrt.v2_0;

import ohos.hdi.nnrt.v2_0.NnrtTypes;

/**
 * @brief 定义模型编译后的对象，包含编译后模型导出、模型推理接口。
 *
 * @since 3.2
 * @version 2.0
 */
interface IPreparedModel {
    /**
     * @brief 导出模型缓存。
     *
     * @param modelCache 模型缓存数组，元素顺序与导出时元素顺序一致，SharedBuffer定义请查看{@link SharedBuffer}。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）
     *
     * @since 3.2
     * @version 2.0
     */
    ExportModelCache([out] struct SharedBuffer[] modelCache);

    /**
     * @brief 获取模型输入支持的维度范围。如果模型输入为固定维度，则维度最大值与最小值相同。
     *
     * @param minInputDims 二维数组存放模型输入数据的最小维度值，数组第1维表示模型输入tensor的数量，第2维表示输入tensor的维度数量。
     * @param maxInputDims 二维数组存放模型输入数据的最大维度值，数组第1维表示模型输入tensor的数量，第2维表示输入tensor的维度数量。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）
     *
     * @since 3.2
     * @version 2.0
     */
    GetInputDimRanges([out] unsigned int[][] minInputDims, [out] unsigned int[][] maxInputDims);

    /**
     * @brief 推理模型
     *
     * @param inputs 模型推理的输入数据，输入的顺序与模型中定义的输入顺序一致，输入数据类型参考IOTensor定义{@link IOTensor}。
     * @param outputs 模型推理的输出数据，推理完后需要往outputs的sharedBuffer中写入输出数据，IOTensor定义请查看{@link IOTensor}。
     * @param outputsDims 模型推理输出数据的形状，输出顺序与outputs顺序一一对应。
     * @param isOutputBufferEnough 模型推理输出数据的SharedBuffer空间是否足够，足够的话返回true，不足则返回false，输出顺序与outputs顺序一一对应。
     *
     * @return 返回0表示成功
     * @return 返回非0表示失败，负数为HDF标准错误码，正数为NNRt定义的专用错误码（请查看{@link NNRT_ReturnCode}）
     *
     * @since 3.2
     * @version 2.0
     */
    Run([in] struct IOTensor[] inputs, [in] struct IOTensor[] outputs, [out] int[][] outputsDims);
}

/** @} */