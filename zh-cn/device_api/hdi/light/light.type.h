/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Light
 * @{
 *
 * @brief 灯模块对灯服务提供通用的接口能力。
 *
 * 灯模块为灯服务提供通用的接口去访问灯驱动。
 * 服务获取灯驱动对象或代理后，可以调用相关的APIs接口获取灯信息、打开或关闭灯，并根据灯类型ID设置灯闪烁模式。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file Light_if.h
 *
 * @brief 定义灯的数据结构，包括灯的状态、灯类型ID、灯的模式、灯的闪烁参数、灯的颜色模式、灯的效果参数和灯的基本信息。
 *
 * @since 3.1
 * @version 1.0
 */

#ifndef LIGHT_TYPE_H
#define LIGHT_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/**
 * @brief 枚举灯的状态值。
 *
 * @since 3.1
 * @version 1.0
 */
enum LightStatus {
    /** 操作成功。 */
    LIGHT_SUCCESS            = 0,
    /** 灯类型ID不支持。 */
    LIGHT_NOT_SUPPORT        = -1,
    /** 设置闪烁不支持。 */
    LIGHT_NOT_FLASH          = -2,
    /** 设置亮度不支持。 */
    LIGHT_NOT_BRIGHTNESS     = -3,
};

/**
 * @brief 枚举灯类型
 *
 * @since 3.1
 * @version 1.0
 */
enum LightId {
    /** 未知ID。 */
    LIGHT_ID_NONE                = 0,
    /** 电源指示灯。 */
    LIGHT_ID_BATTERY             = 1,
    /** 通知灯。 */
    LIGHT_ID_NOTIFICATIONS       = 2,
    /** 报警灯。 */
    LIGHT_ID_ATTENTION           = 3,
    /** 无效ID。 */
    LIGHT_ID_BUTT                = 4,
};

/**
 * @brief 枚举灯的模式
 *
 * @since 3.1
 * @version 1.0
 */
enum LightFlashMode {
    /** 常亮。 */
    LIGHT_FLASH_NONE     = 0,
    /** 闪烁。 */
    LIGHT_FLASH_BLINK    = 1,
    /** 渐变。 */
    LIGHT_FLASH_GRADIENT = 2,
    /** 无效模式。 */
    LIGHT_FLASH_BUTT     = 3,
};

/**
 * @brief 定义闪烁参数。
 *
 * 这些参数包括闪烁模式以及闪烁期间指示灯的打开和关闭时间。
 *
 * @since 3.1
 * @version 1.0
 */
struct LightFlashEffect {
    /** 闪烁模式，详见{@link LightFlashMode}。 */
    int32_t flashMode;
    /** 表示灯在闪烁期间点亮时持续的时间（毫秒）。 */
    int32_t onTime;
    /** 表示灯在闪烁期间熄灭时持续的时间（毫秒）。 */
    int32_t offTime;
};

/**
 * @brief 定义灯的RGB模式。
 *
 * @since 3.2
 * @version 1.0
 */
struct RGBColor {
    /** 亮度值，范围为0-255。 */
    int brightness;
    /** 红色值，范围为0-255。 */
    int r;
    /** 绿色值，范围为0-255。 */
    int g;
    /** 蓝色值，范围为0-255。 */
    int b;
};

/**
 * @brief 定义灯的WRGB模式。
 *
 * @since 3.2
 * @version 1.0
 */
struct WRGBColor {
    /** 白色值，范围为0-255。 */
    int w;
    /** 红色值，范围为0-255。 */
    int r;
    /** 绿色值，范围为0-255。 */
    int g;
    /** 蓝色值，范围为0-255。 */
    int b;
};

/**
 * @brief 定义灯的颜色模式。
 *
 * 参数包括RGB模式和WRGB模式。
 *
 * @since 3.2
 * @version 1.0
 */
union ColorValue {
    /** WRGB模式, 详见{@link WRGBColor}。 */
    struct WRGBColor wrgbColor;
    /** RGB模式, 详见{@link RGBColor}。 */
    struct RGBColor rgbColor;
};

/**
 * @brief 定义亮灯参数。
 *
 * 参数包括灯的模式设置。
 *
 * @since 3.2
 * @version 1.0
 */
struct LightColor {
    /** 设置灯的模式, 详见{@link ColorValue}。 */
    union ColorValue colorValue;
};

/**
 * @brief 定义灯的效果参数。
 *
 * 参数包括亮度和闪烁模式。
 *
 * @since 3.1
 * @version 1.0
 */
struct LightEffect {
    /**
     * 亮度值：Bits 24–31为扩展位，Bits 16–23为红色，Bits 8–15为绿色，Bits 0–7为蓝色。
     * 如果相对应的字节段不等于0，表示打开相应颜色的灯。
    */
    int32_t lightBrightness;
    /** 闪烁模式。详见{@link LightFlashEffect}。 */
    struct LightFlashEffect flashEffect;
};

/**
 * @brief 定义灯的基本信息。
 *
 * 参数包括灯类型ID、逻辑控制器中物理灯的数量、逻辑灯的名称和自定义扩展信息。
 *
 * @since 3.1
 * @version 1.0
 */
struct LightInfo {
    /** 灯类型ID，详见{@link LightId}。 */
    uint32_t lightId;
    /** 逻辑控制器中物理灯的数量。 */
    uint32_t lightNumber;
    /** 逻辑灯的名称。*/
    char lightName[NAME_MAX_LEN];
    /** 自定义扩展信息。 */
    int32_t reserved;
};

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* LIGHT_TYPE_H */
/** @} */

