/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @addtogroup HdiEffect
 * @{
 *
 * @brief Effect模块接口定义。
 *
 * 音效接口涉及数据类型、音效模型接口、音效控制器接口等。
 *
 * @since 4.0
 * @version 1.0
 */
 
 /**
 * @file IEffectModel.idl
 *
 * @brief 音效模型的接口定义文件。
 *
 * 模块包路径：ohos.hdi.audio.effect.v1_0
 *
 * 引用：
 * - ohos.hdi.audio.effect.v1_0.EffectTypes
 * - ohos.hdi.audio.effect.v1_0.IEffectControl
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.audio.effect.v1_0;
import ohos.hdi.audio.effect.v1_0.EffectTypes;
import ohos.hdi.audio.effect.v1_0.IEffectControl;

/**
 * @brief 音效模型接口。
 *
 * 提供音效模型支持的驱动能力，包括获取描述符列表、创建音效控制器、销毁音效控制器、获取指定描述符等。
 *
 * @since 4.0
 * @version 1.0
 */
interface IEffectModel {
    /**
     * @brief 查询供应商/OEM是否提供效果库。
     * 
     * 如果提供，请使用提供的效果库。如果没有，请使用系统服务软件效果。
     *
     * @param model 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param supply 供应商/OEM是否提供效果库的状态。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    IsSupplyEffectLibs([out] boolean supply);

    /**
     * @brief 获取所有支持的音效的描述符。
     *
     * @param model 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param descs 音效描述符列表。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetAllEffectDescriptors([out] struct EffectControllerDescriptor[] descs);

    /**
     * @brief 创建一个用于操作音效实例的音效控制器。
     *
     * @param model 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param info 音效信息。
     * @param contoller 音效控制器对象。
     * @param contollerId 音效控制器ID。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    CreateEffectController([in]struct EffectInfo info, [out] IEffectControl contoller,
                           [out] struct ControllerId id);

    /**
     * @brief 销毁控制器ID指定的音效控制器。
     *
     * @param model 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param contollerId 音效控制器ID。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    DestroyEffectController([in] struct ControllerId id);

    /**
     * @brief 获取指定音效的描述符。
     *
     * @param model 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param effectId 音效ID。
     * @param desc 指定音效的描述符。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetEffectDescriptor([in] String effectId, [out] struct EffectControllerDescriptor desc);
}
/** @} */
